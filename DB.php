<?php
Class DB extends DBDriver{
	
	
	public static function connect($db)
	{
		$sql = new DB();
		if($sql->_driver == 'MYSQL')
		{
			if(empty($db)) $db = _dbWorld; else $db = _dbMain;
			$sql->SQLconnect($db);
		}else if($sql->_driver == 'MONGODB')
		{
			if(empty($db)) $db = _mongoDbWorld; else $db = _mongoDbMain;
			$sql->MongoConnect($db);
		}
	}
	
	public function MongoConnect($data_basename)
	{
		try{
		$con = new MongoClient("mongodb://localhost", array("username" => _mongoUsername, "password" => _mongoPassword));
		$db = $con->selectDB ($data_basename);
		self::$_connection = $db;
		}catch(Exception $e){
			printf("Connect failed: %s\n", "błąd połączenia".$e->getMessage());
			exit();
		}
	}
	
	public static function getDbname()
	{
		$mysqli = self::$_connection;
		if ($result = $mysqli->query("SELECT DATABASE()")) {
				$row = $result->fetch_row();
				$db = "Default database is ".$row[0]."\n" ;
				$result->close();
		}
		return $db;
	}
	
	 public function SQLconnect($data_basename)
    {
		
		$mysqli = new mysqli(_hostname, _username, _password, $data_basename);	

		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
			$mysqli->query("SET NAMES utf8");
			$mysqli->query("SET CHARACTER SET utf8");
			$mysqli->query("SET collation_connection = utf8_polish_ci");
			
	

        self::$_connection = $mysqli;
		
		
    }
	
	public static function getConnection()
    {
        return self::$_connection;
    }
	
	public static function last_id()
	{
		$mysqli = DB::getConnection();
		return $mysqli->insert_id;
	}
	
	public function executeSQL($query)
	{
		$mysqli = self::$_connection;
		$qr = $mysqli->query($query);
		
		if($qr)
		{
			if($this->_insertStatus)
			{
				
				return true;
			}
			else
			{
				if($this->_toArray == 0)
				{
					return $qr->fetch_assoc();
				}
				else
				{
					$d = 0;
					while($k = $qr->fetch_assoc())
					{
						$dat[$d] = $k;
						++$d;
					}
					/*
					if($d == 1)
					{
						return $dat[0];
					}
					*/
					return $dat;
				}
			}
		}else
		{
			printf("Error: %s\n", $mysqli->error);
			echo '<br>';
			echo $query;
			return false;
		}
		
	}
	

	public static function start_commit()
	{
		$mysqli = self::$_connection;
		$mysqli->autocommit(FALSE);
	}
	
	public static function commit()
	{
		$mysqli = self::$_connection;
		$mysqli->commit();
		$mysqli->autocommit(TRUE);
	}
	public static function rollback()
	{
		$mysqli = self::$_connection;
		$mysqli->rollback();
		$mysqli->autocommit(TRUE);
	}
	
	
	public static function close()
	{
		self::$_connection->close();
		//$mysqli = DB::getConnection();
		//$mysqli->close();
		//self::$_connection = null;
	}
	
	public static function string_escape($string)
	{
		$mysqli = self::$_connection;
		return $mysqli->real_escape_string($string);
	}
	
	

	

	
	/* MAIN */
	
	public static function table($table)
	{
		$sql = new DB();
		$sql->_table = $table;
		return $sql;
	}
	
	public function select($select)
	{
		if(is_array($select)){
			foreach($select as $s){
				$this->_select[count($this->_select)] = $s;
			}
		}else{
			$this->_select[count($this->_select)] = $select;
		}
		return $this;
	}
	
	function where()
	{
		$this->_where[count($this->_where)] = func_get_args();
		return $this;
	}
	
	function orWhere()
	{
		$this->_orWhere[count($this->_orWhere)] = func_get_args();
		return $this;
	}
	
	function andOrWhere($dane){//[['start','1'],['start',2]] to jest argument
		$this->_andOrWhere[count($this->_andOrWhere)] = $dane;
		return $this;
	}
	
	public function set()
	{
		$f = func_get_args();
		if(count($f) == 2){
			$this->_set[count($this->_set)] = [$f[0],$f[1]];
		}else{
			foreach ($f[0] as $key => $value) {
				$this->_set[count($this->_set)] = [$key,$value];
			}
		}
		return $this;
	}
	
	public function setPlus($col, $val)
	{
		$this->_setPlus[count($this->_setPlus)] = [$col,$val];
		return $this;
	}
	
	public function setMinus($col, $val)
	{
		$this->_setMinus[count($this->_setMinus)] = [$col,$val];
		return $this;
	}
	
	public function setNOW($col)
	{
		$now = date('Y-m-d H:i:s');
		$this->_set[count($this->_set)] = [$col,$now];
		return $this;
	}
	
	
	
	public function orderBy()
	{
		$this->_orderby[count($this->_orderby)] = func_get_args();
		return $this;
	}
	
	public function groupBy($col)
	{
		$this->_groupBy[count($this->_groupBy)] = $col;
		return $this;
	}
	
	public function take($nb)
	{
		$this->_take = $nb;
		return $this;
	}
	
	public function offset($nb)
	{
		$this->_offset = $nb;
		return $this;
	}
	
	public function sum(){
		$this->_sum[count($this->_sum)] = func_get_args();
		return $this;
	}
	
	public function count(){
		$this->_count[count($this->_count)] = func_get_args();
		return $this;
	}
	
	public function toArray(){
		$this->_toArray = 1;
		return $this;
	}
	
	public function printUpdate()
	{
		
		$this->_insertStatus = true;
		$query = $this->updateDRIVER().$this->_table.$this->setDRIVER().$this->whereDRIVER();
		return $query;
	}
	
	public function update()
	{
		$this->_insertStatus = true;
		$query = $this->updateDRIVER().$this->_table.$this->setDRIVER().$this->whereDRIVER();
		return $this->execute($query);
	}
	
	public function insert()
	{
		$this->_insertStatus = true;
		$query = $this->insertDRIVER().$this->_table.$this->setDRIVER() .$this->whereDRIVER();
		return $this->execute($query);
	}
	
	public function delete()
	{
		$this->_insertStatus = true;
		$query = $this->deleteDRIVER().$this->_table.$this->whereDRIVER();
		return $this->execute($query);
	}
	
	public function get()
	{
		$query = $this->selectDRIVER().$this->fromDRIVER().$this->_table.$this->whereDRIVER().$this->orderByDRIVER().$this->groupByDRIVER().$this->limitDRIVER();;
		return $this->execute($query);
	}
	
	public function prints()
	{
		$query = $this->selectDRIVER().$this->fromDRIVER().$this->_table.$this->whereDRIVER().$this->orderByDRIVER().$this->groupByDRIVER().$this->limitDRIVER();;
		return $query;
		
	}
	
	
	public function execute($query){
		return $this->executeDRIVER($query);
	}
	
	public static function raw($query){
		$sql = new DB();
		$sql->execute($query);
	}
}
