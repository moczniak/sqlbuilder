<?php
Abstract Class DBDriver extends DBSpliter{
	

		/*DRIVER CHOICE */
	
	protected function executeDRIVER($query)
	{
		if($this->_driver == 'MYSQL'){
			return $this->executeSQL($query);
		}else if($this->_driver == 'MONGODB'){
			return $this->executeMNDB($a);
		}
	}
	
	protected function fromDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->fromSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->fromMNDB($a);
		}
	}
	
	protected function deleteDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->deleteSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->deleteMNDB($a);
		}
	}
	
	protected function updateDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->updateSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->updateMNDB($a);
		}
	}
	
	protected function selectDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->selectSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->selectMNDB($a);
		}
	}
	
	protected function insertDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->insertSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->insertMNDB($a);
		}
	}
	
	protected function setDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->setSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->setMNDB($a);
		}
	}
	
	protected function orderByDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->orderBySQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->orderByMNDB($a);
		}
	}
	
	protected function groupByDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->groupBySQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->groupByMNDB($a);
		}
	}
	
	protected function whereDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->whereSQL($a);
		}else if($this->_driver == 'MONGODB'){
			return $this->whereMNDB($a);
		}
	}
	protected function limitDRIVER()
	{
		if($this->_driver == 'MYSQL'){
			return $this->limitSQL();
		}else if($this->_driver == 'MONGODB'){
			return $this->limitMNDB($a);
		}
	}
	

	
	
}