<?php
Abstract Class DBSpliter extends MongoSpliter{
	

	
		
	protected function whereSQL()
	{
		$query = "";
		if(!empty($this->_where)){
			foreach($this->_where as $wh){
				if(empty($query)){
					$query = " WHERE ";
					$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
				}else{
					$query .= " AND ";
					$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
				}
			}
		}else{
			if(!empty($this->_orWhere)){
				foreach($this->_orWhere as $wh)
				{
					if(empty($query)){
						$query = " WHERE ";
						$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
					}else{
						$query .= " OR ";
						$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
					}
				}	
			}			
		}
		if(!empty($query)){
			//AND `been`='0' AND (status='1' OR status='2')"
			
			if(!empty($this->_andOrWhere)){
				foreach($this->_andOrWhere as $ao)
				{
					$split = "";
					foreach($ao as $oa)
					{
						if(empty($split)){
							$split = (count($oa) == 3)? " AND ( `".$oa[0]."`".$oa[1]." '".$oa[2]."'" : " AND ( `".$oa[0]."`= '".$oa[1]."'";
						}else{
							$split .= (count($oa) == 3)? " OR `".$oa[0]."`".$oa[1]." '".$oa[2]."'" : " OR `".$oa[0]."`= '".$oa[1]."'";
						}
					}
					$split .= " )";
				}
				$query =  $query.$split;
			}
			
		}
		return $query;
	}
	
	protected function setSQL()
	{
		$query = "";
		if(!empty($this->_set))
		{
			foreach($this->_set as $ss)
			{
				$ss[1] = "'".$ss[1]."'";
				//$ss[1] = ($ss[1] == 'NOW()')? 'NOW()' : "'".$ss[1]."'";
				if(empty($query))
				{
					$query  = ' SET `'.$ss[0].'` = '.$ss[1];
				}else{
					$query .= ", `".$ss[0].'` = '.$ss[1];
				}
			}
		}
			if(!empty($this->_setPlus))
			{
				foreach($this->_setPlus as $ss)
				{
					if(empty($query))
					{
						$query = " SET `".$ss[0]."`=`".$ss[0]."`+'".$ss[1]."'";
					}else{
						$query .= ", `".$ss[0]."`=`".$ss[0]."`+ '".$ss[1]."'";
					}
				}				
			}
			if(!empty($this->_setMinus))
			{
				foreach($this->_setMinus as $ss)
				{
					if(empty($query))
					{
						$query = " SET `".$ss[0]."`=`".$ss[0]."`-'".$ss[1]."'";
					}else{
						$query .= ", `".$ss[0]."`=`".$ss[0]."`- '".$ss[1]."'";
					}
				}				
			}
		return $query;
	}
	
	protected function orderBySQL()
	{
		$query = "";
		foreach($this->_orderby as $ss){
			if(empty($query)){
				$query = (count($ss) == 2)? ' ORDER BY '.$ss[0].' '.$ss[1] : ' ORDER BY '.$ss[0].' DESC';
			}else{
				$query .= (count($ss) == 2)? ", ".$ss[0].' '.$ss[1] : ", ".$ss[0].' DESC';
			}
		}
		return $query;
	}
	
	protected function groupBySQL()
	{
		$query = "";
		foreach($this->_groupBy as $ss){
			if(empty($query)){
				$query = ' GROUP BY '.$ss;
			}else{
				$query .= ", ".$ss;
			}
		}
		return $query;
	}
	
	protected function limitSQL()
	{
		$query = "";
			if(!empty($this->_take)){
				$query = " LIMIT ";
				$query .= $this->_take;
				if(!empty($this->_offset)){
					$query .= ' OFFSET '.$this->_offset;
				}
			}
		return $query;
	}
	
	protected function fromSQL()
	{
		return " FROM ";
	}
	
	protected function deleteSQL()
	{
		return "DELETE FROM ";
	}
	
	protected function updateSQL()
	{
		return "UPDATE ";
	}
	
	protected function sumSQLsplit()
	{
		$query = "";
		if(!empty($this->_sum)){
			foreach($this->_sum as $ss){
				if(empty($query)){
					$query = (count($ss) == 2)? ' SUM(`'.$ss[0].'`) as '.$ss[1] : ' SUM(`'.$ss[0].'`)';
				}else{
					$query .= (count($ss) == 2)? ", SUM(`".$ss[0].'`) as '.$ss[1] : ", SUM(`".$ss[0].'`)';
				}
			}
		}
		return $query;
	}
	
	protected function countSQLsplit(){
		$query = "";
		if(!empty($this->_count)){
			foreach($this->_count as $ss){
				$ss[0] = ($ss[0] == "*")? $ss[0] : '`'.$ss[0].'`';
				if(empty($query)){
					$query = (count($ss) == 2)? ' COUNT('.$ss[0].') as '.$ss[1] : ' COUNT('.$ss[0].')';
				}else{
					$query .= (count($ss) == 2)? ", COUNT(".$ss[0].') as '.$ss[1] : ", COUNT(".$ss[0].')';
				}
			}
		}
		return $query;
	}
	protected function selectSQL()
	{
		$sum = $this->sumSQLsplit();
		$count = $this->countSQLsplit();
		$query = "";
		if(!empty($this->_select)){
			foreach($this->_select as $ss){
				$ss = ($ss == "*")? $ss : '`'.$ss.'`';
				if(empty($query)){
					$query = 'SELECT '.$ss.'';
				}else{
					$query .= ", ".$ss.'';
				}
			}
			if(!empty($sum)){
				$query .= ", ".$sum;
			}
			if(!empty($count)){
				$query .= ", ".$count;
			}
		}else{
			$split = "";
			
			if(!empty($sum)){
				$split .= $sum;
			}
			if(!empty($count)){
				$split .= (empty($split))? $count : ', '.$count;
			}
			if(!empty($split)){
				$split = "SELECT ".$split;
			}else{
				$split = "SELECT * ";
			}
			//$split = "SELECT * ";
		}
		return $query.$split;
	}
	
	protected function insertSQL()
	{
		return "INSERT INTO ";
	}
	
	
	
}