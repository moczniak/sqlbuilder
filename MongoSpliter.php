<?php
Abstract Class MongoSpliter extends DBvar{
	
		protected function whereMNDB()
	{
		$query = "";
		if(!empty($this->_where)){
			foreach($this->_where as $wh){
				if(empty($query)){
					$query = " WHERE ";
					$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
				}else{
					$query .= " AND ";
					$query .= (count($wh) == 3)? "`".$wh[0]."` ".$wh[1]." '".$wh[2]."'" : "`".$wh[0]."` = '".$wh[1]."'";
				}
			}
		}
		return $query;
	}
	
	protected function setMNDB()
	{
		$query = "";
		if(!empty($this->_set))
		{
			foreach($this->_set as $ss)
			{
				if(empty($query))
				{
					$query = " SET `".$ss[0]."` = '".$ss[1]."'";
				}else{
					$query .= ", `".$ss[0]."` = '".$ss[1]."'";
				}
			}
			if(!empty($this->_setPlus))
			{
				foreach($this->_setPlus as $ss)
				{
					if(empty($query))
					{
						$query = " SET `".$ss[0]."`=`".$ss[0]."`+'".$ss[1]."'";
					}else{
						$query .= ", `".$ss[0]."`=`".$ss[0]."`+ '".$ss[1]."'";
					}
				}				
			}
			if(!empty($this->_setMinus))
			{
				foreach($this->_setMinus as $ss)
				{
					if(empty($query))
					{
						$query = " SET `".$ss[0]."`=`".$ss[0]."`-'".$ss[1]."'";
					}else{
						$query .= ", `".$ss[0]."`=`".$ss[0]."`- '".$ss[1]."'";
					}
				}				
			}
		}
		return $query;
	}
	
	protected function orderByMNDB()
	{
		$query = "";
		foreach($this->_orderby as $ss){
			if(empty($query)){
				$query = ' ORDER BY '.$ss[0].' '.$ss[1];
			}else{
				$query .= ", ".$ss[0].' '.$ss[1];
			}
		}
		return $query;
	}
	
	protected function groupByMNDB()
	{
		$query = "";
		foreach($this->_groupBy as $ss){
			if(empty($query)){
				$query = ' GROUP BY '.$ss;
			}else{
				$query .= ", ".$ss;
			}
		}
		return $query;
	}
	
	protected function limitMNDB()
	{
		$query = "";
			if(!empty($this->_take)){
				$query = " LIMIT ";
				$query .= $this->_take;
				if(!empty($this->_offset)){
					$query .= ' OFFSET '.$this->_offset;
				}
			}
		return $query;
	}
	
	protected function fromMNDB()
	{
		return " FROM ";
	}
	
	protected function deleteMNDB()
	{
		return "DELETE FROM ";
	}
	
	protected function updateMNDB()
	{
		return "UPDATE ";
	}
	
	protected function sumMNDBsplit()
	{
		$query = "";
		if(!empty($this->_sum)){
			foreach($this->_sum as $ss){
				if(empty($query)){
					$query = (count($ss) == 2)? ' SUM(`'.$ss[0].'`) as '.$ss[1] : ' SUM(`'.$ss[0].'`)';
				}else{
					$query .= (count($ss) == 2)? ", SUM(`".$ss[0].'`) as '.$ss[1] : ", SUM(`".$ss[0].'`)';
				}
			}
		}
		return $query;
	}
	
	protected function countMNDBsplit(){
		$query = "";
		if(!empty($this->_count)){
			foreach($this->_count as $ss){
				$ss[0] = ($ss[0] == "*")? $ss[0] : '`'.$ss[0].'`';
				if(empty($query)){
					$query = (count($ss) == 2)? ' COUNT('.$ss[0].') as '.$ss[1] : ' COUNT('.$ss[0].')';
				}else{
					$query .= (count($ss) == 2)? ", COUNT(".$ss[0].') as '.$ss[1] : ", COUNT(".$ss[0].')';
				}
			}
		}
		return $query;
	}
	protected function selectMNDB()
	{
		$sum = $this->sumMNDBsplit();
		$count = $this->countMNDBsplit();
		$query = "";
		if(!empty($this->_select)){
			foreach($this->_select as $ss){
				if(empty($query)){
					$query = 'SELECT `'.$ss.'`';
				}else{
					$query .= ", `".$ss.'`';
				}
			}
			if(!empty($sum)){
				$query .= ", ".$sum;
			}
			if(!empty($count)){
				$query .= ", ".$count;
			}
		}else{
			$split = "";
			
			if(!empty($sum)){
				$split .= $sum;
			}
			if(!empty($count)){
				$split .= (empty($split))? $count : ', '.$count;
			}
			if(!empty($split)){
				$split = "SELECT ".$split;
			}else{
				$split = "SELECT * ";
			}
			//$split = "SELECT * ";
		}
		return $query.$split;
	}
	
	protected function insertMNDB()
	{
		return "INSERT INTO ";
	}
	
	
}
