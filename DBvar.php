<?php


define("_driver", 'MYSQL');


define("_hostname", 'localhost');
define("_username", 'root');
define("_password", 'test');
define("_dbMain", "test");
define("_dbWorld", "test");


define("_mongoHostname", "localhost");
define("_mongoUsername", 'root');
define("_mongoPassword", "test");
define("_mongoDbMain", "test");
define("_mongoDbWorld", "test");

Abstract Class DBvar{
	
	protected static $_connection;
	
	protected $_driver = _driver;
	
	
	protected $_insertStatus = false;
	
	protected $_table;
	protected $_where = array();
	protected $_orWhere = array();
	protected $_andOrWhere = array();
	protected $_set = array();
	protected $_setPlus = array();
	protected $_setMinus = array();
	protected $_select = array();
	protected $_orderby = array();
	protected $_groupBy = array();
	protected $_sum = array();
	protected $_take;
	protected $_offset;
	protected $_toArray = 0;
}